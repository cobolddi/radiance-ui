<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Radiance Renewables</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">
	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3">
				<a href="index.php" class="logo">
					<img src="assets/img/logo.svg" alt="">
				</a>
			</div>
			<div class="col-6 col-md-9">
				<div class="IpadDesktop">
					<nav>
						<ul>
							<li>
								<a href="index.php">HOME</a>
							</li>
							<li class="submenu">
								<a href="who-we-are.php">ABOUT</a>
								<ul>
									<li>
										<a href="who-we-are.php">Who are we?</a>				
									</li>
									<li>
										<a href="team.php">Our Team</a>				
									</li>
								</ul>
							</li>
							<li class="submenu">
								<a href="the-radiance-way.php">THE RADIANCE WAY</a>
								<ul>
									<li>
										<a href="the-radiance-way.php#whyradiance">Why Radiance?</a>				
									</li>
									<li>
										<a href="the-radiance-way.php#USPcards">Our USPs</a>			
									</li>
									<li>
										<a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>				
									</li>
								</ul>
							</li>
							<li>
								<a href="solutions.php">SOLUTIONS</a>
							</li>
							<li>
								<a href="portfolio.php">PORTFOLIO</a>
							</li>
							<li>
								<a href="project-faq.php">FAQs</a>
							</li>
							<li class="contactus submenu">
								<a href="contact-us.php">CONTACT</a>
								<ul>
									<li>
										<a href="contact-us.php#FormBlock">Contact Form</a>				
									</li>
									<li>
										<a href="work-with-us.php">Careers</a>			
									</li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
				<div class="IpadRemoved">
					<div class="MobileMenu">
						<button class="c-hamburger c-hamburger--htx">
					  		<span></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>						
</header>


<nav class="sub-menu open">
	<ul>
		<li>
			<a href="index.php">HOME</a>
		</li>
		<li class="haveSubmenu">
			<a href="who-we-are.php">ABOUT</a>
			<ul>
				<ul>
					<li>
						<a href="who-we-are.php">Who are we?</a>				
					</li>
					<li>
						<a href="team.php">Our Team</a>				
					</li>
				</ul>
			</ul>
		</li>
		<li class="haveSubmenu">
			<a href="the-radiance-way.php">THE RADIANCE WAY</a>
			<ul>
				<li>
					<a href="the-radiance-way.php#whyradiance">Why Radiance?</a>				
				</li>
				<li>
					<a href="the-radiance-way.php#USPcards">Our USPs</a>			
				</li>
				<li>
					<a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>				
				</li>
			</ul>
		</li>
		<li>
			<a href="solutions.php">SOLUTIONS</a>
		</li>
		<li>
			<a href="portfolio.php">PORTFOLIO</a>
		</li>
		<li>
			<a href="project-faq.php">FAQs</a>
		</li>
		<li class="haveSubmenu">
			<a href="contact-us.php">CONTACT</a>
			<ul>
				<li>
					<a href="contact-us.php#FormBlock">Contact Form</a>				
				</li>
				<li>
					<a href="work-with-us.php">Careers</a>			
				</li>
			</ul>
		</li>
	</ul>
</nav>

<main>

<section class="ContactUsSection">
	<div class="container">
		<div class="ContactAddress">
			<div class="FormBlock" id="FormBlock" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="350">
				<h2 class="LiteOrangeBorderBottom GreyText">Contact Form</h2>
				<p>Please chose your area of input and fill out the form below to contact us.</p>
				<h6>We look forward to hearing from you!</h6>
				<form action="">
					<div class="row">
						<div class="col-12 col-md-12">
							<label for="">Subject</label>
							<select>
								<option value="">Project Enquiry</option>
								<option value="">Issues or Feedback about an existing Radiance Renewable project</option>
								<option value="">General / Others</option>
							</select>
						</div>
						<div class="col-12 col-md-6">
							<label for="">First Name*</label>
							<input type="text" placeholder="Enter your First Name" required="">
						</div>
						<div class="col-12 col-md-6">
							<label for="">Last Name*</label>
							<input type="text" placeholder="Enter your Last Name" required="">
						</div>
						<div class="col-12 col-md-6">
							<label for="">Email Address*</label>
							<input type="email" placeholder="Enter your Email Address" required="">
						</div>
						<div class="col-12 col-md-6">
							<label for="">Name of Organisation</label>
							<input type="text" placeholder="Enter your Organisation">
						</div>
						<div class="col-12 col-md-12">
							<label for="">Contact Number</label>
							<input type="text" placeholder="Enter your Contact Number">
						</div>
						<div class="col-12 col-md-12">
							<label for="">Message</label>
							<input type="text" placeholder="Enter your Message">
						</div>
						<div class="col-12 col-md-12">
							<input type="submit" value="Submit">
						</div>
					</div>
				</form>
			</div>
			<div class="AddressBlock" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="350">
				<h2>Address</h2>
				<h6>Our Offices</h6>
				<p>One Indiabulls Centre<br>16th floor Tower 2A<br>
					Senapati Bapat Marg<br>
					Elphinstone Road<br> 
					Mumbai - 400013
				</p>	
				<p class="phone">Phone: <a href="tel:+912240436000">+912240436000</a></p>
				<p>Email: <a href="mailto:contact@radiancerenewables.com">contact@radiancerenewables.com</a></p>
			</div>
		</div>
	</div>										
</section>


<section class="GoogleMapSection">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3772.2967202781333!2d72.83076016427519!3d19.00664235908945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7ceebf90bf831%3A0xcebc69977489f729!2sOne%20India%20Bulls%20Centre%20Tower%202A!5e0!3m2!1sen!2sin!4v1614075616690!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</section>

<?php @include('template-parts/footer.php') ?>