$(window).on('load', function () {
  AOS.refresh();
});
$(function () {
  AOS.init();
});

var headerHeight;

function headerHeightFn(){
  headerHeight = $('#header').outerHeight(true);
    console.log("Header Height", headerHeight);
    $('main').css('paddingTop', headerHeight);
}

// Locate to other page's specific id
  function toOtherPageSpecificId() {
    var hashcode = window.location.hash;
    if(hashcode) {
      console.log('hashcode', hashcode)
      $('html,body').animate({
        scrollTop: $(''+hashcode).offset().top - (headerHeight)
      },'slow');
    }
  }


//Smooth scroll click on <a> tag
  function smoothScrollingToSection(){
    $('#header nav ul li a[href^="#"]').click(function(){
      console.log("Working or not");
      var target = $(this).attr("href");
      $('#header nav ul li a.active').removeClass('active')
      $(this).addClass('active')
      $('html, body').animate({
          scrollTop: $( $(this).attr('href') ).offset().top - (headerHeight)
      }, 100);
      return false;
    });
    // $(window).scroll(function() {
    //   var scrollDistance = $(window).scrollTop();
    //   $('.stickyArea > .sectionActive').each(function(i) {
    //     if ($(this).position().top - (headerHeight + innerPageNavHeight + 20) <= scrollDistance) {
    //         $('nav>ul>li>a.active').removeClass('active');
    //         $('nav>ul>li>a').eq(i).addClass('active');
    //     }
    //   });
    // }).scroll();
  }

$(document).ready(function() {
  
  headerHeightFn();
  smoothScrollingToSection();
  //toOtherPageSpecificId();


// $("nav").find("a").click(function(e) {
//     // e.preventDefault();
//     var section = $(this).attr("href");
//     $("html, body").animate({
//         scrollTop: $(section).offset().top
//     });
// });




// Accordion
    $('.accordion').find('.accordion__title').click(function(e){
      e.preventDefault();
      // Adds active class
      $(this).toggleClass('active');
      // Expand or collapse this panel
      $(this).next().slideToggle('fast');
      // Hide the other panels
      $('.accordion__content').not($(this).next()).slideUp('fast');
      // Removes active class from other titles
      $('.accordion__title').not($(this)).removeClass('active');    
    });


  $('.toggle').click(function(e) {
      $(".toggle").removeClass("active");
      $(this).toggleClass("active");
      e.preventDefault();
    
      let $this = $(this);
    
      if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
      } else {
          $this.parent().parent().find('li .inner').removeClass('show');
          $this.parent().parent().find('li .inner').slideUp(350);
          $this.next().toggleClass('show');
          $this.next().slideToggle(350);
      }
  });


  // Team Popup
  $('.open-popup-link').magnificPopup({
    type: 'inline',
    midClick: true,
    mainClass: 'mfp-with-zoom', // this class is for CSS animation below

    zoom: {
      enabled: true, // By default it's false, so don't forget to enable it

      duration: 300, // duration of the effect, in milliseconds
      easing: 'ease-in-out', // CSS transition easing function

      // The "opener" function should return the element from which popup will be zoomed in
      // and to which popup will be scaled down
      // By defailt it looks for an image tag:
      opener: function(openerElement) {
        // openerElement is the element on which popup was initialized, in this case its <a> tag
        // you don't need to add "opener" option if this code matches your needs, it's defailt one.
        return openerElement.is('img') ? openerElement : openerElement.find('img');
      }
    }
  });

  // Gallery Popup
  $('.image-popup-vertical-fit').magnificPopup({
      type: 'image',
      mainClass: 'mfp-with-zoom', 
      gallery:{
          enabled:true
        },

      zoom: {
        enabled: true, 

        duration: 300, // duration of the effect, in milliseconds
        easing: 'ease-in-out', // CSS transition easing function

        opener: function(openerElement) {

          return openerElement.is('img') ? openerElement : openerElement.find('img');
      }
    }

    });


  // Homebanner Slider
  $('.Homeslider').slick({
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      fade: true,
      autoplaySpeed: 2000,
      arrows: true,
      responsive: [{
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
         breakpoint: 400,
         settings: {
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1
         }
      }]
  });


  // Articles Slider
  $('.Articlesslider').slick({
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 2000,
      arrows: true,
      responsive: [{
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
         breakpoint: 767,
         settings: {
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
         }
      }]
  });

  // Lifecycle Slider
  $('.LifecycleSlider').slick({
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 2000,
      adaptiveHeight: true,
      arrows: false,
      responsive: [{
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
         breakpoint: 400,
         settings: {
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1
         }
      }]
  });

  // Testimonial Slider
  $('.testimonialslider').slick({
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: true,
      responsive: [{
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
         breakpoint: 400,
         settings: {
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1
         }
      }]
  });


	//-----HeaderPadding
    var headerHeight = $('#header').outerHeight(true);
    // console.log("Header Height", headerHeight);
    $('main').css('paddingTop', headerHeight);
	
	$(function() {
			$('li').has('ul').mouseover(function(){
			$(this).children('ul').css('visibility','visible');
			}).mouseout(function(){
			$(this).children('ul').css('visibility','hidden');
			})      
	 });  

	// Menu Overlay Animation
	var toggles = document.querySelectorAll(".c-hamburger");

      for (var i = toggles.length - 1; i >= 0; i--) {
        var toggle = toggles[i];
        toggleHandler(toggle);
      };

      function toggleHandler(toggle) {
        toggle.addEventListener("click", function(e) {
          e.preventDefault();
          if (this.classList.contains("is-active") === true) {
            this.classList.remove("is-active");
            $('.open').removeClass('oppenned');
          } else {
            this.classList.add("is-active");
            $(".open").addClass('oppenned');
          }
        });
      }
    $(".sub-menu li a").click(function(event) {
        $(".open").removeClass('oppenned');
        $(".c-hamburger").removeClass('is-active');
    });

    let checkScrennSize = function() {
      if (window.matchMedia('(max-width: 991px)').matches) {
          $(".haveSubmenu>a").append('<span><img src="assets/img/arw-down-white.svg"></span>');
      }
      $(".haveSubmenu").find('a>span').click(function(e){
      // $(".haveSubmenu>a>span").click(function(e){
        e.stopPropagation();
        e.preventDefault();
        $(this).parents('a').parent().toggleClass("Active_MobileDropdown");
      });
    }
    checkScrennSize();




})




