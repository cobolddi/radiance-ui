<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageHeader/TheRadianceBanner.php') ?>

<?php @include('template-parts/CenterAlignContent.php') ?>

<section class="Section GradientBlock NewGradientBlock">
	<div class="container">
		<div class="TopWhiteContentOnly">
			<h2 class="WhiteText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">If you are looking for</h2>
			<div class="row">
				<div class="col-12 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
					<ul>
						<li><strong>A holistic, reliable long-term partner</strong> that offers customised, cost-effective and safe solutions;</li>
						<li><strong>A thought-leader</strong> in the areas of knowledge, research, and state-of-the-art technology;</li>
						<li><strong>A competitively priced firm</strong> offering value for money, and flexibility through different business models;</li>
					</ul>
				</div>
				<div class="col-12 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
					<ul>
						<li><strong>A socially and environmentally conscious organisation</strong> that takes great pride in creating positive change for all stakeholders involved;</li>
						<li><strong>A well-funded set-up</strong>, with access to capital and international knowledge;</li>
						<li><strong>A trustworthy ally</strong> who is transparent about contractual obligations, payment structures, & costs;</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="ContentWithLink">
			<p>Then Radiance Renewables is the right choice! <a href="contact-us.php" target="_blank">Click here</a> to get in touch with our experts.</p>
		</div>
	</div>
</section>

<section class="OverlflowImageWithOurusp" id="USPcards">
	<div class="container">
		<div class="TopImage" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="450">
			<picture>
				<source media="(min-width:465px)" srcset="assets/img/tempimg/our-usp.png">
				<img src="assets/img/window.png" alt="Radiance Renewable">
			</picture>
		</div>
		<div class="UspCardsWithHeading">
			<div class="SmallContainer">
				<div class="CenterHeading">
					<h2 class="LiteOrangeBorderBottom">Our USP</h2>
					<p>Radiance Renewables re-defines how we engage and harvest green energy solutions for our customers through the way we plan, design, engineer, implement, and maintain our projects. Regardless of size, location or technology, we guarantee the following four competitive advantages in every undertaking:</p>
				</div>
			</div>	
			<div class="UspCardsBlock">
				<div class="row">
					<div class="col-12 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
						<div class="Cards">
							<div class="TopOrangeHeading" >
								<h4>
									<img src="assets/img/solutions.svg" alt="">
									<span>Customized Solutions</span>
								</h4>
							</div>
							<div class="BottomWhiteContent">
								<p>Every project is unique, and our teams will develop a customized solution right from the planning phase. Radiance Renewables does not believe in a one-size-fits-all’-approach. Once the bespoke project is set up, our teams take over the full management of each project, including operations, asset management and maintenance, allowing our customers to focus on what matters most: running and growing their business.</p>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
						<div class="Cards">
							<div class="TopOrangeHeading" >
								<h4>
									<img src="assets/img/safety.svg" alt="">
									<span>Safety and Transparency</span>
								</h4>
							</div>
							<div class="BottomWhiteContent">
								<p>Radiance Renewables follows international safety measures across all project stages. In addition, we ensure upfront transparency and highest levels of governance - including a zero tolerance approach towards bribery and corruption - and utmost clarity on all relevant process steps in close co-operation with our customers and partners to ensure a productive and goal-oriented work environment.</p>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
						<div class="Cards">
							<div class="TopOrangeHeading" >
								<h4>
									<img src="assets/img/technology.svg" alt="">
									<span>Cutting-Edge Technology <br> and Design Thinking</span>
								</h4>
							</div>
							<div class="BottomWhiteContent">
								<p>Radiance Renewables instills its signature 360-Degree Design Thinking process at all stages of the project. This ensures a customer-centric planning model, allowing it to solve potential hurdles in a creative and innovative way. Through our team and network of leading industry and technology experts, we ensure the use of future-proof and state-of-the-art technology in all our project.</p>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
						<div class="Cards">
							<div class="TopOrangeHeading" >
								<h4>
									<img src="assets/img/finance.svg" alt="">
									<span>Flexible Financing and<br> Unique Parentage</span>
								</h4>
							</div>
							<div class="BottomWhiteContent">
								<p>Competitively priced, flexible business models that include innovative CAPEX and OPEX options that focuses on providing energy solutions under a “pay as you go” model to our customers thereby reducing customer investments and project performance risks.</p>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-12" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
						<div class="Cards">
							<div class="TopOrangeHeading" >
								<h4>
									<img src="assets/img/lineage.svg" alt="">
									<span>Unique Lineage</span>
								</h4>
							</div>
							<div class="BottomWhiteContent IconBlock">
								<p>Radiance Renewables is owned by a climate-focused fund with institutional investors and sponsors. With our sharp focus on climate and environment, Radiance Renewables is proud to have adopted the following UN Sustainable Development goals:</p>
								<ul>
									<li>
										<img src="assets/img/tempimg/g12.png" alt="">
										<p>SDG12: Responsible Consumption and Production</p>
									</li>
									<li>
										<img src="assets/img/tempimg/g13.png" alt="">
										<p>SDG13: Climate Action</p>
									</li>
									<li>
										<img src="assets/img/tempimg/g9.png" alt="">
										<p>SDG9: Industry, Innovation and Infrastructure</p>
									</li>
									<li>
										<img src="assets/img/tempimg/g7.png" alt="">
										<p>SDG7: Affordable and Clean Energy</p>
									</li>
									<li>
										<img src="assets/img/tempimg/g11.png" alt="">
										<p>SDG11: Sustainable Cities and Communities</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="AllLineage" id="RadianceProcess">
				<a href="who-we-are.php#lineage">Click here to read more about Radiance’s Lineage.</a>
			</div>
		</div>
	</div>
</section>

<section class="Section ContainerLeftImageRightContent">
	<div class="container">
		<div class="Topheading" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
			<h2 class="LiteOrangeBorderBottom">The Radiance Process</h2>
		</div>
		<div class="LeftImgRightContent" style="background: url(assets/img/tempimg/process.png) no-repeat;">
			<div class="LeftImg IpadRemoved">
				<img src="assets/img/tempimg/process.png" alt="">
			</div>
			<div class="RightContent"  data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
				<p>At Radiance Renewables, we work towards a goal that – with the help of us and other stakeholders ― will sooner rather than later revolutionise the way business is conducted in India’s current renewable energy market:</p>
				<h4>We aim to overcome short-term thinking.</h4>
				<p>Radiance focuses on life-cycle-cost of energy (LCOE) based business models which takes away the risks associated with engaging in short term thinking in the way projects are designed, constructed, partnerships are being built, contracts are structured and output is measured. We engage with our customers as a true partner and not merely as a project vendor.</p>
				<p>Therefore, we instill a long-term vision in even the smallest ways we engage with every single one of our potential and existing customers, partners, investors, and team members. Through this process, our mission is to be a leading force in India’s green energy revolution that inspires trust, transparency, and the vision to collaborate for a better, environmentally conscious future.</p>
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php') ?>