<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Radiance Renewables</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">
    
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-6 col-md-3">
                <a href="index.php" class="logo">
                    <img src="assets/img/logo.svg" alt="">
                </a>
            </div>
            <div class="col-6 col-md-9">
                <div class="IpadDesktop">
                    <nav>
                        <ul>
                            <li>
                                <a href="index.php">HOME</a>
                            </li>
                            <li class="submenu">
                                <a href="who-we-are.php">ABOUT</a>
                                <ul>
                                    <li>
                                        <a href="who-we-are.php">Who are we?</a>                
                                    </li>
                                    <li>
                                        <a href="team.php">Our Team</a>             
                                    </li>
                                </ul>
                            </li>
                            <li class="submenu">
                                <a href="the-radiance-way.php">THE RADIANCE WAY</a>
                                <ul>
                                    <li>
                                        <a href="the-radiance-way.php#whyradiance">Why Radiance?</a>                
                                    </li>
                                    <li>
                                        <a href="the-radiance-way.php#USPcards">Our USPs</a>            
                                    </li>
                                    <li>
                                        <a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>             
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="solutions.php">SOLUTIONS</a>
                            </li>
                            <li>
                                <a href="portfolio.php">PORTFOLIO</a>
                            </li>
                            <li>
                                <a href="project-faq.php">FAQs</a>
                            </li>
                            <li class="contactus submenu">
                                <a href="contact-us.php">CONTACT</a>
                                <ul>
                                    <li>
                                        <a href="contact-us.php#FormBlock">Contact Form</a>             
                                    </li>
                                    <li>
                                        <a href="work-with-us.php">Careers</a>          
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="IpadRemoved">
                    <div class="MobileMenu">
                        <button class="c-hamburger c-hamburger--htx">
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>                      
</header>


<nav class="sub-menu open">
    <ul>
        <li>
            <a href="index.php">HOME</a>
        </li>
        <li class="haveSubmenu">
            <a href="who-we-are.php">ABOUT</a>
            <ul>
                <ul>
                    <li>
                        <a href="who-we-are.php">Who are we?</a>                
                    </li>
                    <li>
                        <a href="team.php">Our Team</a>             
                    </li>
                </ul>
            </ul>
        </li>
        <li class="haveSubmenu">
            <a href="the-radiance-way.php">THE RADIANCE WAY</a>
            <ul>
                <li>
                    <a href="the-radiance-way.php#whyradiance">Why Radiance?</a>                
                </li>
                <li>
                    <a href="the-radiance-way.php#USPcards">Our USPs</a>            
                </li>
                <li>
                    <a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>             
                </li>
            </ul>
        </li>
        <li>
            <a href="solutions.php">SOLUTIONS</a>
        </li>
        <li>
            <a href="portfolio.php">PORTFOLIO</a>
        </li>
        <li>
            <a href="project-faq.php">FAQs</a>
        </li>
        <li class="haveSubmenu">
            <a href="contact-us.php">CONTACT</a>
            <ul>
                <li>
                    <a href="contact-us.php#FormBlock">Contact Form</a>             
                </li>
                <li>
                    <a href="work-with-us.php">Careers</a>          
                </li>
            </ul>
        </li>
    </ul>
</nav>

<main>

<?php @include('template-parts/pageHeader/InsideBanner.php') ?>

<?php @include('template-parts/RightElementLeftContent.php') ?>

<?php @include('template-parts/TopIconBottomContent.php') ?>

<section class="Section GreyBgSection ContainerLeftImgRightContent" id="lineage">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="imgWrap">
                    <img src="assets/img/logo-1.png" alt="">
                    <div class="logobox">
                        <img src="assets/img/logo-2.png" alt="">
                        <img src="assets/img/logo-3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="contentWrap">
                    <h2 class="LiteOrangeBorderBottom GreyText">The Radiance Lineage</h2>
                    <p><strong>Radiance Renewables was incorporated in 2018</strong> as a private equity owned developer of competitive renewable energy solutions for commercial, industrial and residential customers, enabling them to achieve their sustainability goals.</p>
                    <p>Radiance Renewables is a 100% subsidiary of the Green Growth Equity Fund (GGEF), an Alternative Investment Fund managed by EverSource Capital, with cornerstone investments from India’s National Investment and Infrastructure Fund (NIIF) and the Foreign Commonwealth Development Office (FCDO) of the UK government.</p>
                    <p><a href="https://www.eversourcecapital.com/" target="_blank">EverSource Capital</a> is a joint venture between <a href="https://www.everstonecapital.com/" target="_blank">Everstone Capital</a> and <a href="https://www.lightsourcebp.com/" target="_blank">Lightsource BP</a>, a global leader in renewables, with a portfolio of more than 3 GW under operation and a 16 GW development pipeline across 14 countries.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="left_image_right_content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 IpadView" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
              <div class="imgWrap">
                  <img src="assets/img/tempimg/bulb.png" alt="gift_image">
              </div>
            </div>
            <div class="col-md-6 IpadView">
                <div class="contentWrap mr-5" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="550">
                    <h2 class="LiteOrangeBorderBottom GreyText">Energised by Innovation</h2>
                    <p>At Radiance Renewables, we believe that innovation is a result of vision, creativity, ambition, and ― in our case ― high-end engineering. Our approach to delivering renewable energy solutions is powered by innovation across all the stages of our projects. We flexibly adapt to all challenges and resolve them quickly, enabling our teams to move forward with momentum and energy. At the core of the Radiance Renewables business lies the intrinsic will to explore new pathways, as well as a deep-rooted fascination for technology and alternative energy solutions. </p>
                    <p>This unique mix of passions drives all the departments of Radiance Renewables to constantly re-invent and improve their products and approaches with the help of the latest technology, out-of-the-box design thinking, and world-class engineering, resulting in the best solutions for partners, investors, and clients.</p>
                </div>
            </div>            
        </div>
    </div>
</section>

<!-- <section class="left_image_right_content WhiteBgSection CenterLeftElement">
	<div class="row">
		<div class="col-md-6 MobileOnly" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
          <div class="imgWrap">
              <img src="assets/img/tempimg/solar-panel.png" alt="">
          </div>
        </div>

        <div class="col-md-6">
            <div class="contentWrap text-left-md" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
                <h2 class="LiteOrangeBorderBottom GreyText">Energised by Innovation</h2>
                <p>At Radiance Renewables, we believe that innovation is a result of vision, creativity, ambition, and ― in our case ― high-end engineering. Our approach to delivering renewable energy solutions is powered by innovation across all the stages of our projects. We flexibly adapt to all challenges and resolve them quickly, enabling our teams to move forward with momentum and energy. At the core of the Radiance Renewables business lies the intrinsic will to explore new pathways, as well as a deep-rooted fascination for technology and alternative energy solutions. </p>
                <p>This unique mix of passions drives all the departments of Radiance Renewables to constantly re-invent and improve their products and approaches with the help of the latest technology, out-of-the-box design thinking, and world-class engineering, resulting in the best solutions for partners, investors, and clients.</p>
            </div>
        </div>

        <div class="col-md-6 DesktopOnly" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">
          <div class="imgWrap">
              <img src="assets/img/tempimg/solar-panel.png" alt="">
          </div>
        </div>
	</div>
</section> -->

<!-- <section class="left_image_right_content RightBottomElement">
	<div class="row">
		<div class="col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
	      <div class="imgWrap">
	          <img src="assets/img/tempimg/sunset.png" alt="gift_image">
	      </div>
	    </div>
	    <div class="col-md-6">
	        <div class="contentWrap mr-5">
	            <h2 class="LiteOrangeBorderBottom GreyText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">We follow four strict policies:</h2>
	            <div class="row">
	            	<div class="col-6 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
	            		<div class="IconContent">
	            			<img src="assets/img/corporate.svg" alt="">
	            			<h4>Corporate Governance</h4>
	            			<p>Zero tolerance approach and adoption of global anti-bribery and anti-corruption laws</p>
	            		</div>
	            	</div>
                    <div class="col-6 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="500">
                        <div class="IconContent">
                            <img src="assets/img/structure.svg" alt="">
                            <h4>Structured Offerings</h4>
                            <p>Bespoke solutions available to suit different customer requirements</p>
                        </div>
                    </div>
	            	<div class="col-6 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
	            		<div class="IconContent">
	            			<img src="assets/img/green-power.svg" alt="">
	            			<h4>Lowest Tariffs on green power</h4>
	            			<p> Achieved through our strategic relationships with Lightsource BP</p>
	            		</div>
	            	</div>
                    <div class="col-6 col-md-6" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="500">
                        <div class="IconContent">
                            <img src="assets/img/health.svg" alt="">
                            <h4>Health, Safety and Environment</h4>
                            <p> International standards of HSE and IFC sustainable development goals adopted</p>
                        </div>
                    </div>
	            </div>
	        </div>
	    </div>
	</div>
</section> -->	

<section class="left_image_right_content LiteOrangeSection pb7">
	<div class="row">
		<div class="col-md-6 IpadRemoved" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">
          <div class="imgWrap">
              <img src="assets/img/tempimg/vision.png" alt="">
          </div>
        </div>

        <div class="col-md-6 IpadLeftView">
            <div class="contentWrap text-left-md">
                <div class="LeftIconRightContent">
                    <h2 class="LiteOrangeBorderBottom GreyText" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">Our Vision</h2>
                	<ul data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
                		<li>
                			<div class="Icon">
                				<img src="assets/img/lead.svg" alt="">
                			</div>
                			<div class="Content">
                				<h2>We lead.</h2>
                				<p>At Radiance Renewables, we aim to be an innovative, forward-thinking and technology-driven leader in renewable energy solutions for commercial, industrial and residential consumers in India.</p>
                			</div>
                		</li>
                		<li>
                			<div class="Icon">
                				<img src="assets/img/partner.svg" alt="">
                			</div>
                			<div class="Content">
                				<h2>We partner.</h2>
                				<p>Our mission is to build long-lasting partnerships that continue to deliver turn-key renewable energy solutions in India through insight, innovation, trust & technology - The Radiance Way</p>
                			</div>
                		</li>
                		<li>
                			<div class="Icon">
                				<img src="assets/img/enable.svg" alt="">
                			</div>
                			<div class="Content">
                				<h2>We eco-enable.</h2>
                				<p>Our clients can rely on us to support them in building their own sustainable, green brand identity. This approach guarantees a win-win situation for our clients as well as for Radiance Renewables, as it improves both the firm’s reputation and performance.</p>
                			</div>
                		</li>
                	</ul>
                </div>
            </div>
        </div>

        <div class="col-md-6 IpadDesktop" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">
          <div class="imgWrap">
              <img src="assets/img/tempimg/vision.png" alt="">
          </div>
        </div>
	</div>
</section>		



<?php @include('template-parts/footer.php') ?>