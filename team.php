<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Radiance Renewables</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">
	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3">
				<a href="index.php" class="logo">
					<img src="assets/img/logo.svg" alt="">
				</a>
			</div>
			<div class="col-6 col-md-9">
				<div class="IpadDesktop">
					<nav>
						<ul>
							<li>
								<a href="index.php">HOME</a>
							</li>
							<li class="submenu">
								<a href="who-we-are.php">ABOUT</a>
								<ul>
									<li>
										<a href="who-we-are.php">Who are we?</a>				
									</li>
									<li>
										<a href="team.php">Our Team</a>				
									</li>
								</ul>
							</li>
							<li class="submenu">
								<a href="the-radiance-way.php">THE RADIANCE WAY</a>
								<ul>
									<li>
										<a href="the-radiance-way.php#whyradiance">Why Radiance?</a>				
									</li>
									<li>
										<a href="the-radiance-way.php#USPcards">Our USPs</a>			
									</li>
									<li>
										<a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>				
									</li>
								</ul>
							</li>
							<li>
								<a href="solutions.php">SOLUTIONS</a>
							</li>
							<li>
								<a href="portfolio.php">PORTFOLIO</a>
							</li>
							<li>
								<a href="project-faq.php">FAQs</a>
							</li>
							<li class="contactus submenu">
								<a href="contact-us.php">CONTACT</a>
								<ul>
									<li>
										<a href="contact-us.php#FormBlock">Contact Form</a>				
									</li>
									<li>
										<a href="work-with-us.php">Careers</a>			
									</li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
				<div class="IpadRemoved">
					<div class="MobileMenu">
						<button class="c-hamburger c-hamburger--htx">
					  		<span></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>						
</header>


<nav class="sub-menu open">
	<ul>
		<li>
			<a href="index.php">HOME</a>
		</li>
		<li class="haveSubmenu">
			<a href="who-we-are.php">ABOUT</a>
			<ul>
				<ul>
					<li>
						<a href="who-we-are.php">Who are we?</a>				
					</li>
					<li>
						<a href="team.php">Our Team</a>				
					</li>
				</ul>
			</ul>
		</li>
		<li class="haveSubmenu">
			<a href="the-radiance-way.php">THE RADIANCE WAY</a>
			<ul>
				<li>
					<a href="the-radiance-way.php#whyradiance">Why Radiance?</a>				
				</li>
				<li>
					<a href="the-radiance-way.php#USPcards">Our USPs</a>			
				</li>
				<li>
					<a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>				
				</li>
			</ul>
		</li>
		<li>
			<a href="solutions.php">SOLUTIONS</a>
		</li>
		<li>
			<a href="portfolio.php">PORTFOLIO</a>
		</li>
		<li>
			<a href="project-faq.php">FAQs</a>
		</li>
		<li class="haveSubmenu">
			<a href="contact-us.php">CONTACT</a>
			<ul>
				<li>
					<a href="contact-us.php#FormBlock">Contact Form</a>				
				</li>
				<li>
					<a href="work-with-us.php">Careers</a>			
				</li>
			</ul>
		</li>
	</ul>
</nav>

<main>

<section class="HomeBanner InsideBanner InsideBigBanner">
	<picture>
		<source media="(min-width:465px)" srcset="assets/img/tempimg/teambanner.png">
		<img src="assets/img/tempimg/team.jpg" alt="Radiance Renewable">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="BannerText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="700">
				<h1 class="OrangeBorderBottom">Our Team</h1>
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/TeamCardsBlock.php') ?>

<?php @include('template-parts/footer.php') ?>