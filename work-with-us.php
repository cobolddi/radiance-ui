<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Radiance Renewables</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">
	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3">
				<a href="index.php" class="logo">
					<img src="assets/img/logo.svg" alt="">
				</a>
			</div>
			<div class="col-6 col-md-9">
				<div class="IpadDesktop">
					<nav>
						<ul>
							<li>
								<a href="index.php">HOME</a>
							</li>
							<li class="submenu">
								<a href="who-we-are.php">ABOUT</a>
								<ul>
									<li>
										<a href="who-we-are.php">Who are we?</a>				
									</li>
									<li>
										<a href="team.php">Our Team</a>				
									</li>
								</ul>
							</li>
							<li class="submenu">
								<a href="the-radiance-way.php">THE RADIANCE WAY</a>
								<ul>
									<li>
										<a href="the-radiance-way.php#whyradiance">Why Radiance?</a>				
									</li>
									<li>
										<a href="the-radiance-way.php#USPcards">Our USPs</a>			
									</li>
									<li>
										<a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>				
									</li>
								</ul>
							</li>
							<li>
								<a href="solutions.php">SOLUTIONS</a>
							</li>
							<li>
								<a href="portfolio.php">PORTFOLIO</a>
							</li>
							<li>
								<a href="project-faq.php">FAQs</a>
							</li>
							<li class="contactus submenu">
								<a href="contact-us.php">CONTACT</a>
								<ul>
									<li>
										<a href="contact-us.php#FormBlock">Contact Form</a>				
									</li>
									<li>
										<a href="work-with-us.php">Careers</a>			
									</li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
				<div class="IpadRemoved">
					<div class="MobileMenu">
						<button class="c-hamburger c-hamburger--htx">
					  		<span></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>						
</header>


<nav class="sub-menu open">
	<ul>
		<li>
			<a href="index.php">HOME</a>
		</li>
		<li class="haveSubmenu">
			<a href="who-we-are.php">ABOUT</a>
			<ul>
				<ul>
					<li>
						<a href="who-we-are.php">Who are we?</a>				
					</li>
					<li>
						<a href="team.php">Our Team</a>				
					</li>
				</ul>
			</ul>
		</li>
		<li class="haveSubmenu">
			<a href="the-radiance-way.php">THE RADIANCE WAY</a>
			<ul>
				<li>
					<a href="the-radiance-way.php#whyradiance">Why Radiance?</a>				
				</li>
				<li>
					<a href="the-radiance-way.php#USPcards">Our USPs</a>			
				</li>
				<li>
					<a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>				
				</li>
			</ul>
		</li>
		<li>
			<a href="solutions.php">SOLUTIONS</a>
		</li>
		<li>
			<a href="portfolio.php">PORTFOLIO</a>
		</li>
		<li>
			<a href="project-faq.php">FAQs</a>
		</li>
		<li class="haveSubmenu">
			<a href="contact-us.php">CONTACT</a>
			<ul>
				<li>
					<a href="contact-us.php#FormBlock">Contact Form</a>				
				</li>
				<li>
					<a href="work-with-us.php">Careers</a>			
				</li>
			</ul>
		</li>
	</ul>
</nav>

<main>

<section class="HomeBanner InsideBanner InsideBigBanner">
	<picture>
		<source media="(min-width:465px)" srcset="assets/img/tempimg/workbanner.png">
		<img src="assets/img/tempimg/work-with-us.jpg" alt="Radiance Renewable">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="BannerText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="700">
				<h1 class="OrangeBorderBottom">Work with us</h1>
			</div>
		</div>
	</div>
</section>

<section class="Section RecruitmentSection">
	<div class="container SmallContainer">
		<div class="TopHeading">
			<h2 class="LiteOrangeBorderBottom">Welcome to the Radiance Renewables recruitment section!</h2>
			<p>Thank you for your interest in working with us. Radiance Renewables is a fast growing player in India’s dynamic solar industry, and we are always looking for hard-working bright minds with a can-do attitude to join our team.</p>
			<a href="https://www.linkedin.com/company/radiance-renewables" class="OrangeYellowButton" target="_blank"><span>Visit our LinkedIn page</span></a>
		</div>
		<div class="ThreeCardsBlock">
			<div class="LeftIconRightContentBox" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
				<div class="LeftIcon">
					<img src="assets/img/culture.svg" alt="">
					<h4>Our Culture</h4>
				</div>
				<div class="RightContentBox">
					<p>Radiance Renewables is a dynamic organisation with an open office culture, and flat hierarchies. Thus, we are proud to have created an environment that fosters high productivity, responsibility, and professional excellence. This culture provides a strong foundation for achieving the performance our customers and partners deserve and expect.</p>
				</div>
			</div>
			<div class="LeftIconRightContentBox" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
				<div class="LeftIcon">
					<img src="assets/img/belief.svg" alt="">
					<h4>Our Beliefs</h4>
				</div>
				<div class="RightContentBox">
					<p>At Radiance Renewables, we believe in creating a self- motivating, empowering, trustworthy, and responsible team. Our management puts strong focus on integrity, and embraces diversity throughout all divisions of the firm. In addition, the Radiance Renewables leadership team frequently updates the company’s goals, progress, and achievements to practice transparency, and to boost creative cooperation and co-creation across all the firm’s verticals.</p>
				</div>
			</div>
			<div class="LeftIconRightContentBox" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="500">
				<div class="LeftIcon">
					<img src="assets/img/candidate.svg" alt="">
					<h4>What we are looking for in candidates</h4>
				</div>
				<div class="RightContentBox">
					<p>Working at Radiance Renewables means working in a company which</p>
					<ul>
						<li>Encourages its employees to think out of the box, and find innovative solutions</li>
						<li>Embraces challenges, and thrives on tackling them with curiosity and expertise</li>
						<li>Values and appreciates all its team members</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="Section ApplyHereSection">
	<div class="container">
		<div class="Topheading">
			<h2 class="WhiteBorderBottom">Apply Here</h2>
		</div>
		<div class="ApplyFormBlock" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
			<form action="">
				<div class="row">
					<div class="col-12 col-md-4">
						<label for="">Position applied for</label>
						<input type="text" placeholder="Enter your Position">
					</div>
					<div class="col-12 col-md-4">
						<label for="">Candidate Name</label>
						<input type="text" placeholder="Enter your Name">
					</div>
					<div class="col-12 col-md-4">
						<label for="">Qualifications</label>
						<input type="text" placeholder="Enter your Qualifications">
					</div>
					<div class="col-12 col-md-8">
						<label for="">University Name</label>
						<input type="text" placeholder="Enter your University Name">
					</div>
					<div class="col-12 col-md-4">
						<label for="">University Date Graduation</label>
						<input type="text" placeholder="Enter your University Date Graduation">
					</div>
					<div class="col-12 col-md-4">
						<label for="">Current Organisation Name</label>
						<input type="text" placeholder="Enter your Current Organisation Name">
					</div>
					<div class="col-12 col-md-4">
						<label for="">Period & Tenure of current employment</label>
						<input type="text" placeholder="Enter your Period & Tenure">
					</div>
					<div class="col-12 col-md-4">
						<label for="">Experience (in total years)</label>
						<input type="text" placeholder="Enter your Experience">
					</div>
					<div class="col-12 col-md-4">
						<label for="">Current Designation</label>
						<input type="text" placeholder="Enter your Current Designation">
					</div>
					<div class="col-12 col-md-4">
						<label for="">Contact Number</label>
						<input type="text" placeholder="Enter your Contact number">
					</div>
					<div class="col-12 col-md-4">
						<label for="">Email Id</label>
						<input type="email" placeholder="Enter your Email id">
					</div>
					<div class="col-12 col-md-8">
						<label for="">Brief Message from the Candidate</label>
						<input type="text" placeholder="Enter your message" class="message">
					</div>
					<div class="col-12 col-md-4">
						<input type="submit" value="Submit">
					</div>
				</div>		
			</form>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php') ?>