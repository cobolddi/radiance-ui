<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Radiance Renewables</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">
	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3">
				<a href="index.php" class="logo">
					<img src="assets/img/logo.svg" alt="">
				</a>
			</div>
			<div class="col-6 col-md-9">
				<div class="IpadDesktop">
					<nav>
						<ul>
							<li>
								<a href="index.php">HOME</a>
							</li>
							<li class="submenu">
								<a href="who-we-are.php">ABOUT</a>
								<ul>
									<li>
										<a href="who-we-are.php">Who are we?</a>				
									</li>
									<li>
										<a href="team.php">Our Team</a>				
									</li>
								</ul>
							</li>
							<li class="submenu">
								<a href="the-radiance-way.php">THE RADIANCE WAY</a>
								<ul>
									<li>
										<a href="the-radiance-way.php#whyradiance">Why Radiance?</a>				
									</li>
									<li>
										<a href="the-radiance-way.php#USPcards">Our USPs</a>			
									</li>
									<li>
										<a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>				
									</li>
								</ul>
							</li>
							<li>
								<a href="solutions.php">SOLUTIONS</a>
							</li>
							<li>
								<a href="portfolio.php">PORTFOLIO</a>
							</li>
							<li>
								<a href="project-faq.php">FAQs</a>
							</li>
							<li class="contactus submenu">
								<a href="contact-us.php">CONTACT</a>
								<ul>
									<li>
										<a href="contact-us.php#FormBlock">Contact Form</a>				
									</li>
									<li>
										<a href="work-with-us.php">Careers</a>			
									</li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
				<div class="IpadRemoved">
					<div class="MobileMenu">
						<button class="c-hamburger c-hamburger--htx">
					  		<span></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>						
</header>


<nav class="sub-menu open">
	<ul>
		<li>
			<a href="index.php">HOME</a>
		</li>
		<li class="haveSubmenu">
			<a href="who-we-are.php">ABOUT</a>
			<ul>
				<ul>
					<li>
						<a href="who-we-are.php">Who are we?</a>				
					</li>
					<li>
						<a href="team.php">Our Team</a>				
					</li>
				</ul>
			</ul>
		</li>
		<li class="haveSubmenu">
			<a href="the-radiance-way.php">THE RADIANCE WAY</a>
			<ul>
				<li>
					<a href="the-radiance-way.php#whyradiance">Why Radiance?</a>				
				</li>
				<li>
					<a href="the-radiance-way.php#USPcards">Our USPs</a>			
				</li>
				<li>
					<a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>				
				</li>
			</ul>
		</li>
		<li>
			<a href="solutions.php">SOLUTIONS</a>
		</li>
		<li>
			<a href="portfolio.php">PORTFOLIO</a>
		</li>
		<li>
			<a href="project-faq.php">FAQs</a>
		</li>
		<li class="haveSubmenu">
			<a href="contact-us.php">CONTACT</a>
			<ul>
				<li>
					<a href="contact-us.php#FormBlock">Contact Form</a>				
				</li>
				<li>
					<a href="work-with-us.php">Careers</a>			
				</li>
			</ul>
		</li>
	</ul>
</nav>

<main>

<section class="HomeBanner InsideBanner PortfolioBanner InsideBigBanner">
	<picture>
		<source media="(min-width:465px)" srcset="assets/img/tempimg/porfolio-banner.png">
		<img src="assets/img/tempimg/Portfolio.jpg" alt="Radiance Renewable">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="BannerText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="700">
				<h1 class="OrangeBorderBottom">Portfolio</h1>
			</div>
		</div>
	</div>
</section>

<section class="Section ArticlesSection">
	<div class="container">
		<div class="TopHeading">
			<h2 class="LiteOrangeBorderBottom">Articles & Interviews</h2>
		</div>
		<div class="ArticlesBlock">
			<div class="Articlesslider">
			  	<div class="Cards">
					<div class="TopImg">
						<img src="assets/img/tempimg/article.png" alt="">
					</div>
					<div class="BottomContent">
						<h4>Aiming for 1.5GW of renewables</h4>
						<a href="https://renewablewatch.in/2020/06/01/aiming-for-1-5-gw-of-renewables-2/" class="OrangeYellowButton" target="_blank"><span>Read more</span></a>
					</div>
				</div>
				<div class="Cards">
					<div class="TopImg">
						<img src="assets/img/tempimg/article-img.png" alt="">
					</div>
					<div class="BottomContent">
						<h4>BP to Invest Us $70 Million in Green Growth Equity Fund (GGEF)</h4>
						<a href="https://www.eversourcecapital.com/bp-invest-us-70-million-green-growth-equity-fund-ggef/" class="OrangeYellowButton" target="_blank"><span>Read more</span></a>
					</div>
				</div>
				<div class="Cards">
					<div class="TopImg">
						<img src="assets/img/tempimg/article-img-3.jpg" alt="">
					</div>
					<div class="BottomContent">
						<h4>Radiance Renewables engages Prescinto to monitor its solar assets</h4>
						<a href="https://economictimes.indiatimes.com/industry/energy/power/radiance-renewables-engages-prescinto-to-monitor-its-solar-assets/articleshow/79169893.cms" class="OrangeYellowButton" target="_blank"><span>Read more</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="Section LiteOrangeSection ProjectsPipelineTables">
	<div class="container">
		<div class="TopHeading">
			<h2 class="LiteOrangeBorderBottom">Existing Projects & Pipeline</h2>
		</div>
		
		<!-- <div class="TableBox">
			<table>
				<thead>
					<tr>
						<th class="Type">Type</th>
						<th class="Size">Size (MWp)</th>
						<th class="Customers">Off-Takers / Customers</th>
						<th class="State">State</th>
					</tr>
				</thead>

				<tbody>
					<tr class="tablerow">
					    <td class="Type">Open Access Ground mount</td>
					    <td class="Size">21.00</td>
					    <td class="Customers">
					    	<ul class="CircleBullet">
								<li>Large Japanese auto manufacturer</li>
								<li>Leading manufacturer and supplier of precision engineered products</li>
								<li>Leading exporter of FIBC products</li>
								<li>Leading Indian manufacturer of valve seat inserts and turbo charger parts for IC engines</li>
							</ul>
					    </td>
					    <td class="State">Karnataka</td>
					</tr>
					<tr class="tablerow">
					    <td class="Type">Open Access Ground mount</td>
					    <td class="Size">12.00</td>
					    <td class="Customers">
					    	<ul class="CircleBullet">
								<li>Most preferred OE Manufacturer and supplier for global auto majors</li>
							</ul>
					    </td>
					    <td class="State">Tamil Nadu</td>
					</tr>
					<tr class="tablerow">
					    <td class="Type">Behind the Meter (ground mount & rooftop)</td>
					    <td class="Size">1.75</td>
					    <td class="Customers">
					    	<ul class="CircleBullet">
								<li>One of the largest synthetic yarn spinning plants in India</li>
								<li>One leading manufacturer of ball joints for steering systems and suspension components in the country. Its products are used in passenger cars, jeeps and light commercial vehicles.</li>
							</ul>
					    </td>
					    <td class="State">Rajasthan & Tamil Nadu</td>
					</tr>
					<tr class="tablerow">
					    <td class="Type">Rooftop</td>
					    <td class="Size">0.52</td>
					    <td class="Customers">
					    	<ul class="CircleBullet">
								<li>Multiple off-takers in commercial and residential segment</li>
							</ul>
					    </td>
					    <td class="State">Multiple</td>
					</tr>
					<tr class="tablerow">
					    <td class="Type">Rooftop</td>
					    <td class="Size">0.18</td>
					    <td class="Customers">
					    	<ul class="CircleBullet">
								<li>Educational Institute – Engineering and Technology</li>
							</ul>
						</td>
					    <td class="State">Chattisgarh</td>
					</tr>
					<tr class="tablerow">
					    <td class="Type">Open Access Ground Mount Asset Management Services only</td>
					    <td class="Size">60.00</td>
					    <td class="Customers">
					    	<ul class="CircleBullet">
								<li>Solar Energy Corporation of India (SECI)</li>
							</ul>
						</td>
					    <td class="State">Maharashtra</td>
					</tr>
					<tr class="tablerow">
					    <td class="Type">TOTAL</td>
					    <td class="Size">95.45</td>
					    <td class="Customers"></td>
					    <td class="State"></td>
					</tr>
				</tbody>	
			</table>
		</div> -->
		<div class="PipelineProjects">
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="Cards">
						<div class="TopOrangeHeading">
							<h4>Open Access Ground mount</h4>
							<ul class="EnergyState">
								<li>
									<img src="assets/img/power.svg" alt="">
									<span>21.00 (MWp)</span>
								</li>
								<li>
									<img src="assets/img/state.svg" alt="">
									<span>Karnataka</span>
								</li>
							</ul>
						</div>
						<div class="BottomWhiteContent">
							<ul class="CircleBullet">
								<li>Large Japanese auto manufacturer</li>
								<li>Leading manufacturer and supplier of precision engineered products</li>
								<li>Leading exporter of FIBC products</li>
								<li>Leading Indian manufacturer of valve seat inserts and turbo charger parts for IC engines</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Cards">
						<div class="TopOrangeHeading">
							<h4>Open Access Ground mount</h4>
							<ul class="EnergyState">
								<li>
									<img src="assets/img/power.svg" alt="">
									<span>12.00 (MWp)</span>
								</li>
								<li>
									<img src="assets/img/state.svg" alt="">
									<span>Tamil Nadu</span>
								</li>
							</ul>
						</div>
						<div class="BottomWhiteContent">
							<ul class="CircleBullet">
								<li>Most preferred OE Manufacturer and supplier for global auto majors</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Cards">
						<div class="TopOrangeHeading">
							<h4>Behind the Meter <br>(ground mount and rooftop)</h4>
							<ul class="EnergyState">
								<li>
									<img src="assets/img/power.svg" alt="">
									<span>1.75 (MWp)</span>
								</li>
								<li>
									<img src="assets/img/state.svg" alt="">
									<span>Rajasthan & Tamil Nadu</span>
								</li>
							</ul>
						</div>
						<div class="BottomWhiteContent">
							<ul class="CircleBullet">
								<li>One of the largest synthetic yarn spinning plants in India</li>
								<li>One leading manufacturer of ball joints for steering systems and suspension components in the country. Its products are used in passenger cars, jeeps and light commercial vehicles.</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Cards">
						<div class="TopOrangeHeading">
							<h4>Rooftop</h4>
							<ul class="EnergyState">
								<li>
									<img src="assets/img/power.svg" alt="">
									<span>0.52 (MWp)</span>
								</li>
								<li>
									<img src="assets/img/state.svg" alt="">
									<span>Multiple</span>
								</li>
							</ul>
						</div>
						<div class="BottomWhiteContent">
							<ul class="CircleBullet">
								<li>Multiple off-takers in commercial and residential segment</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Cards">
						<div class="TopOrangeHeading">
							<h4>Rooftop</h4>
							<ul class="EnergyState">
								<li>
									<img src="assets/img/power.svg" alt="">
									<span>0.18 (MWp)</span>
								</li>
								<li>
									<img src="assets/img/state.svg" alt="">
									<span>Chattisgarh</span>
								</li>
							</ul>
						</div>
						<div class="BottomWhiteContent">
							<ul class="CircleBullet">
								<li>Educational Institute – Engineering and Technology</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Cards">
						<div class="TopOrangeHeading">
							<h4>Open Access Ground Mount Asset Management Services only</h4>
							<ul class="EnergyState">
								<li>
									<img src="assets/img/power.svg" alt="">
									<span>60.00 (MWp)</span>
								</li>
								<li>
									<img src="assets/img/state.svg" alt="">
									<span>Maharashtra</span>
								</li>
							</ul>
						</div>
						<div class="BottomWhiteContent">
							<ul class="CircleBullet">
								<li>Solar Energy Corporation of India (SECI)</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="Section WhiteBgSection TopContentWithGallery">
	<div class="container">
		<div class="TopHeading">
			<h2 class="LiteOrangeBorderBottom">CSR / Giving Back</h2>
		</div>
		<div class="TopContentBox">
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="Content">
						<p>At Radiance Renewables, we take pride in pro-actively leaving a positive mark, and of being conscious of our social and environmental impact on the world. Our policy is simple: with every decision we make, we consider the effect of the same on all the stakeholders involved.</p>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Content">
						<p>We call this process <span>‘Creating Shared Value’</span>: While constantly innovating and improving economical solutions for our clients, investors and partners, we simultaneously generate benefits for the people who work with us, and for the nature and wildlife that surrounds our projects.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="Content">
						<h2 class="LiteOrangeBorderBottom">COVID-19 Response in Karnataka</h2>
						<p>When times get tough, the first instinct is to hold on to resources. However, at Radiance Renewables we decide to take the opposite route and extend our hand to share what we have. When the initial spread of COVID-19 slowed down life and business across the country, Radiance Renewables, together with its local O&M partner <a href="https://www.rayspowerinfra.com/" target="_blank">Rays Power Infra</a> distributed 64 grocery packages in the Gurmgera Panchayat in Karnataka, where both firms operate a 21MW solar project together. Each parcel contained rice, cooking oil, and spices, and the activity was well received by the local community.</p>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Content">
						<img src="assets/img/tempimg/covid.jpeg" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="GalleryBox">
			<div class="row">
				<div class="col-6 col-md-4">
					<div class="magnific-img">
						<a class="image-popup-vertical-fit" href="assets/img/tempimg/CSR1.png">
							<img src="assets/img/tempimg/CSR1.png" alt="9.jpg" />
						</a>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="magnific-img">
						<a class="image-popup-vertical-fit" href="assets/img/cerificate.png">
							<img src="assets/img/cerificate.png" alt="9.jpg" />
						</a>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div class="magnific-img">
						<a class="image-popup-vertical-fit" href="assets/img/tempimg/CSR2.png">
							<img src="assets/img/tempimg/CSR2.png" alt="9.jpg" />
						</a>
					</div>
				</div>
			</div>
		</div>	
	</div>
</section>

<?php @include('template-parts/TestimonialsSlider.php') ?>

<?php @include('template-parts/footer.php') ?>