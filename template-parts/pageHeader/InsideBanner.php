<section class="HomeBanner InsideBanner">
	<picture>
		<source media="(min-width:465px)" srcset="assets/img/tempimg/whoweare.png">
		<img src="assets/img/tempimg/who-we-are.jpg" alt="Radiance Renewable">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="BannerText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="700">
				<h1 class="OrangeBorderBottom">ABOUT</h1>
			</div>
		</div>
	</div>
</section>