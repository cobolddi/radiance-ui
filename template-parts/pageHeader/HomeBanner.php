<section class="HomeBanner">	
	<div class="Homeslider">
	  	<div class="Cards">
			<picture>
				<source media="(min-width:465px)" srcset="assets/img/homebanner-2.jpg">
				<img src="assets/img/homebanner-mobile-2.jpg" alt="Radiance Renewable">
			</picture>
			<div class="BannerContent">
				<div class="container">
					<div class="BannerText">
						<h4>Radiance Renewables</h4>
						<h1 class="OrangeBorderBottom">Energised by Innovation</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="Cards">
			<picture>
				<source media="(min-width:465px)" srcset="assets/img/homebanner.png">
				<img src="assets/img/homebanner-mobile.png" alt="Radiance Renewable">
			</picture>
			<div class="BannerContent">
				<div class="container">
					<div class="BannerText">
						<h4>Radiance Renewables</h4>
						<h1 class="OrangeBorderBottom">Energised by Innovation</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="Cards">
			<picture>
				<source media="(min-width:465px)" srcset="assets/img/hombanner-3.jpg">
				<img src="assets/img/homebanner-3.jpg" alt="Radiance Renewable">
			</picture>
			<div class="BannerContent">
				<div class="container">
					
				</div>
			</div>
		</div>
	</div>
</section>