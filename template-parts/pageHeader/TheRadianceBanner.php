<section class="HomeBanner InsideBanner PortfolioBanner InsideBigBanner">
	<picture>
		<source media="(min-width:465px)" srcset="assets/img/tempimg/radiancebanner.png">
		<img src="assets/img/tempimg/the-radience-way.jpg" alt="Radiance Renewable">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="BannerText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="700">
				<h1 class="OrangeBorderBottom">The Radiance Way</h1>
			</div>
		</div>
	</div>
	<div class="RadianceBanner" id="whyradiance">
		
	</div>
</section>