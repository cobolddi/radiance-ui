<section class="OverlflowImageWithIconBox">
	<div class="container">
		<div class="TopImage" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="100">
			<picture>
				<source media="(min-width:465px)" srcset="assets/img/solar-window.png">
				<img src="assets/img/window.png" alt="Radiance Renewable">
			</picture>
		</div>
		<div class="LeftIconsRightImage">
			<div class="IconBox">
				<h2 data-aos="fade-in" data-aos-easing="linear" data-aos-duration="450">Impact of our sustainable Business Solutions</h2>
				<h4 data-aos="fade-in" data-aos-easing="linear" data-aos-duration="480">In approx. numbers<br> (As on November, 2020)</h4>
				<div class="row">
					<div class="col-6 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="120">
						<div class="IconWithHeading">
							<div class="Icon">
								<img src="assets/img/emission-big.svg" alt="">
							</div>
							<h4>32,000</h4>
							<p><span>(in tonnes)</span><br> Emissions Avoided</p>
						</div>
					</div>
					<div class="col-6 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="150">
						<div class="IconWithHeading">
							<div class="Icon">
								<img src="assets/img/water-saved-big.svg" alt="">
							</div>
							<h4>64,000</h4>
							<p><span>(in cubic meters)</span><br> Water Saved</p>
						</div>
					</div>
					<div class="col-6 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="180">
						<div class="IconWithHeading">
							<div class="Icon">
								<img src="assets/img/jobs-big.svg" alt="">
							</div>
							<h4>34</h4>
							<p><span>(in absolute numbers)</span><br> Jobs Sustained</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>