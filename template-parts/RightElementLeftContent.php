<section class="RightElementLeftContent">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
				<div class="LeftContentBlock">
					<h2 class="OrangeBorderBottom GreyText">Who are we?</h2>
					<p>Radiance Renewables is one of India’s fastest-growing renewable energy solution providers. Incorporated in 2018 as a technology and innovation leader, it aims to provide custom built affordable solutions that deliver economic savings to customers. Continuously expanding strategically, it is an organisation that is built on four key pillars.</p>
				</div>
			</div>
			<div class="col-12 col-md-4" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">
				<div class="RightImg">
					<img src="assets/img/pinkelement.svg" alt="">
				</div>
			</div>
		</div>
	</div>
</section>