<section class="Section LiteOrangeSection TestimonialSlider">
	<div class="container">
		<div class="TopHeading">
			<h2 class="LiteOrangeBorderBottom">Testimonial</h2>
		</div>
		<div class="Slider">
			<div class="testimonialslider">
			  	<div class="TestimonialBlock">
			  		<p class="testimonial">As a corporate social responsibility policy, AT India Auto Parts Private Limited is committed to adopting environment-friendly systems, and reducing its carbon foot print. Radiance Renewables has done an excellent job for us. We are completely satisfied by the way its teams have put in all effort for the commencement of the project on time.</p>
			  		<h4>Hirofumi Kume</h4>
			  		<p class="designation">Managing Director <br><span>At India Auto Parts Pvt Ltd</span></p>
			  	</div>
			</div>
		</div>
	</div>
</section>