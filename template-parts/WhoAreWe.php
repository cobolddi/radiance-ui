<section class="Section WhoWeAre">
	<div class="container">
		<div class="TopHeading" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="200">
			<h2 class="OrangeBorderBottom GreyText">Who are we?</h2>
			<p>Radiance Renewables is one of India’s fastest-growing renewable energy solution providers. Incorporated in 2018 as a technology and innovation leader we are a 100% subsidiary of the Green Growth Equity Fund (GGEF), an Alternative Investment Fund managed by EverSource Capital, with cornerstone investments from India’s National Investment and Infrastructure Fund (NIIF) and the Foreign Commonwealth Development Office (FCDO) of the UK government. With our deep-rooted fascination for technology, we are proud to deliver renewable energy solutions that are powered by innovation across all the stages of our projects.</p>
			<a href="who-we-are.php" class="OrangeYellowButton"><span>Read More</span></a>
		</div>
		<!-- <div class="TopIconBottomHeading">
			<div class="row">
				<div class="col-6 col-md-3" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="300">
					<div class="IconWithHeading">
						<div class="IconBox">
							<img src="assets/img/economic.svg" alt="">
						</div>
						<h4>Vibrancy</h4>
					</div>
				</div>
				<div class="col-6 col-md-3" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="350">
					<div class="IconWithHeading">
						<div class="IconBox">
							<img src="assets/img/resilience.svg" alt="">
						</div>
						<h4>Resilience</h4>
					</div>
				</div>
				<div class="col-6 col-md-3" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="400">
					<div class="IconWithHeading">
						<div class="IconBox">
							<img src="assets/img/reliability.svg" alt="">
						</div>
						<h4>Reliability</h4>
					</div>
				</div>
				<div class="col-6 col-md-3" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="450">
					<div class="IconWithHeading">
						<div class="IconBox">
							<img src="assets/img/eco-enable.svg" alt="">
						</div>
						<h4>Eco-Enabling</h4>
					</div>
				</div>
			</div>
		</div>	 -->		
	</div>
</section>