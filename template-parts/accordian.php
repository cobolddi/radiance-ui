<section class="Section AccordionSection">
    <div class="container">
        <div class="wrap" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
            <h2>
                <span><img src="assets/img/solar-big.svg" alt=""></span>
                Solar Basics
            </h2>
            <ul class="accordion">
                <li class="accordion__item">
                    <a class="accordion__title" href="">How does a solar plant work?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>It is quite simple: The panels of a solar power plant are made of special silicon-based Photovoltaic (PV) cells, which convert sunlight into electricity. The generated electricity is turned into a Direct Current (DC), and further converted into an Alternating Current (AC) by the plant inverter. After the conversion phase, the electricity is fed into your existing electrical network, and from thereon contributes to power your factory. </p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">Does a solar plant produce power at night?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>No, solar panels can’t produce any electricity post sunset. During night-time, all required power loads are derived from the standard power grid or battery/generators. However, when the sun rises again in the morning, the solar plant starts to automatically produce electricity again. Read more about this in <a href="#powersupply">What is my Power Supply Guarantee?</a></p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">Will my solar plant generate power when the weather is cloudy, rainy, or snowy days?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>As long as sunlight with a suitable intensity falls on the solar panels, your plant will continue to generate power. However, rain, fog, sandstorms, and snow can reduce its power output.</p>                        
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">What are my financial savings on electricity?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>Setting up a solar solution in partnership with Radiance Renewables comes with many benefits. The most obvious, however, is that you will be able to make significant savings with Radiance Renewables as compared to buying electricity off the grid.</p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">How do I know the output of my solar plant?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>Every Radiance Renewable solar plant is provided with a smart monitoring system which continuously checks the plant's performance in real time. Thus, you can easily monitor your plant’s output on the go, among other statistics, via a dedicated app on your smartphone! Please visit our Radiance Live Ticker to see some statistics of our installed plants in real time!</p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">Who covers for damages caused by storms, typhoons, earthquakes, etc.?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>Our teams at Radiance Renewables support you in closing on the optimal insurance option for your situation, in order to mitigate potential financial loss due to damages of expensive solar plant components caused by storms, typhoons, earthquakes, and so on. <a href="contact-us.php" target="_blank">Get in touch</a> with our project experts to learn more!</p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">What happens when the utility power is interrupted (blackout/shutdown)?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>If your solar plant shut downs in case of grid power failure (as per utility norms/regulations), the plant automatically (subject to local electricity regulations) starts to operate as soon as the grid power returns. In case you are willing to connect a Diesel Generator (DG) to back-up the plant, a customised system can be provided by our Radiance Renewables engineers that allows you to run your plant parallelly with a DG solution. <a href="contact-us.php" target="_blank">Get in touch</a> with our project experts to learn more!</p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="wrap" id="rightsolution" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
            <h2>
                <span><img src="assets/img/house-big.svg" alt=""></span>
                Is solar the right solution for me?
            </h2>
            <ul class="accordion">
                <li class="accordion__item">
                    <a class="accordion__title" href="">What can I do with the unused power produced by my solar plant?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>Don’t worry ― net metering is the answer! This process guarantees that any excess or unused energy units produced by your solar project can be fed into the grid, from where they can be acquired by Distribution Companies (DISCOM) at a pre-determined price. Please note that these options are only available in certain States. <a href="contact-us.php" target="_blank">Get in touch</a> with our project experts to learn more!</p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">Which solar energy solution is best for me?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>Depending on your energy requirements and your available space, our Radiance Renewables experts will advise you about one of the two following options:</p>
                            <ul class="bullets">
                                <li>Rooftop or On-Premise Solution
                                    <ol>
                                        <p>A rooftop solution guarantees the supply of solar energy to your plants and offices by installing on-site (or “within premise”) solar plants. The power is produced on-site and, therefore, reduces the electricity consumption from the grid. At the same time, your solar rooftop solution seamlessly connects directly to your facilities, guaranteeing a 100% stable supply during the day and at night. This approach is ideal for residential complexes, such as buildings or bungalows, small industrial sheds, and warehouses.</p>
                                        <p>Advantages:</p>
                                            <ol>
                                                <li><strong>Sustainable</strong>: Rooftop solutions are generally more cost-effective and eco-friendly compared to energy supplied via the common grid. Additionally, solar tariffs can be locked for long term usage, while grid tariffs are expected to rise.</li>
                                                <li><strong>Economical</strong>: Depending on state regulations, rooftop solar customers have the option to bank or sell surplus energy. </li>
                                                <li><strong>Independent</strong>: Rooftop solutions offer a lower reliance on the common grid supply, and guarantee continuous power during the day, regardless of grid performance.</li>
                                                <li><strong>Asset Usage</strong>: This approach efficiently uses a company’s idling assets and, therefore, makes the most of all available space.</li>
                                                <li><strong>Requirement</strong>: On-Premise Solutions need available rooftop space, or any other idling assets, such as parking areas, of approximately 10 square meters per Kilowatt.</li>
                                            </ol>
                                    </ol>
                                </li>
                                <li>Ground-mounted Open Access Solution
                                    <ol>
                                        <p>A ground-mounted open access solution guarantees the supply of green energy to your plants and offices by purchasing power from the open market through an offsite solar plant. The power produced there is sent into the common grid, and then can be acquired.</p>
                                        <p>Advantages:</p>
                                            <ol>
                                                <li><strong>Intrusion-free</strong>: Open Access Solutions are more cost-effective than the regular grid supply, and offer the supply of solar energy to your premise without intrusion of the roof or other facilities.</li>
                                                <li><strong>Flexible</strong>: In addition, this kind of solar project gives the flexibility to structure the required energy supply in the most cost-effective way.</li>
                                                <li><strong>Limitless</strong>: Open Access Solutions offer the maximum use of Renewable Energy that is possible, and does not limit any extra or subsequent requirements due to space or production capacity.</li>
                                                <li><strong>Requirement</strong>: Open Access Solutions needs a minimum of 4 acres of land per Megawatt to set up a project.</li>
                                                <li><strong>Permissions</strong>: Setting up and running an Open Access Solution requires permission from the Utility Board, depending on the location.</li>
                                            </ol>
                                        
                                        <p>Please <a href="contact-us.php" target="_blank">contact our experts</a> to learn more!</p>
                                    </ol>
                                </li>                                
                            </ul>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">What happens when/if the installed technology becomes outdated in a few years from now?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>Our Radiance Renewables solar systems are designed for a lifespan of a minimum of 25 years. With the help of our dedicated maintenance program under the supervision of our technical  teams, Radiance Renewables ensures the plant’s efficiency over the years.</p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">Is it possible to carry out maintenance on my roof after a solar plant is installed?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>Absolutely! Appropriate solar panels ― or any other associated equipment of your solar plant ― can be temporarily and safely removed by our Radiance Renewables technical teams, if and when required.</p>
                    </div>
                </li>
                <li class="accordion__item" id="powersupply">
                    <a class="accordion__title" href="">How is the safety of my roof ensured during solar power installations?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>To kick-start the customised design process, our teams at Radiance Renewables will run a simulation based on the location of your site as well as on a specific analysis of the solar radiation for your area. This process includes a detailed roof structure study by our structural experts. Every suggested solution ensures a design that neither damages your roof nor structurally compromises it throughout the lifecycle of the solar plant. Furthermore, we guarantee that the installation process is exclusively handled by authorised and credible Radiance Renewables personnel only, who follow industrial safety best-practises.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="wrap" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="500">
            <h2>
                <span><img src="assets/img/solution-big.svg" alt=""></span>
                Radiance's Solutions
            </h2>
            <ul class="accordion">
                <li class="accordion__item">
                    <a class="accordion__title" href="">What are the different business models offered by Radiance Renewables for solar projects?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>Radiance Renewables is your trusted partner for your solar project. We offer maximum flexibility to conceptualise, design, implement, manage, and maintain a business model that is customised to your unique needs and requirements.</p>
                        <p>In general, we differentiate between two key approaches:</p>
                        <ul class="bullets">
                            <li><strong>OPEX</strong> – You undertake only the operating expenses, and a certain amount of equity investment in the solar project. </li>
                            <li><strong>CAPEX</strong> – You undertake all capital investment, operating expenses and, therefore, own the solar project.</li>
                        </ul>
                        <p>The following table gives you a great overview of the two different models in three relevant decision-making categories.</p>
                        <div class="TableBox">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="Type"></th>
                                        <th class="Size">OPEX Model </th>
                                        <th class="Customers">CAPEX Model </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr class="tablerow">
                                        <td class="Type">Ownership & Project Development</td>
                                        <td class="Size">
                                            <ul class="CircleBullet">
                                                <li>Radiance Renewables develops, owns, and operates the solar project.</li>
                                                <li>Customer buys electricity from Radiance Renewables under PPA tariff (fixed or variable) against supplied energy units.</li>
                                                <li>The option of project buyback is available.</li>
                                            </ul>
                                        </td>
                                        <td class="Customers">
                                            <ul class="CircleBullet">
                                                <li>The project is developed and operated by Radiance Renewables. </li>
                                                <li>The project is owned by the customer from the start of operation.</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr class="tablerow">
                                        <td class="Type">Financing</td>
                                        <td class="Size">
                                            <ul class="CircleBullet">
                                                <li>A combination of equity (from Radiance Renewables and customer in case of captive/group captive projects), and debt.</li>
                                                <li>Lenders provide for the debt through a tri-party agreement between the customer, the developer (Radiance Renewables), and the lender.</li>
                                            </ul>
                                        </td>
                                        <td class="Customers">
                                            <ul class="CircleBullet">
                                                <li>An upfront amount (percentage) is committed by the customer. The remaining equity is brought in by Radiance Renewables. The customer pays Radiance Renewables a monthly annuity, excluding O&M services, during the tenure of the Deferred Capex period.</li>
                                                <li>Lenders provide for the debt through a tri-party agreement between the customer, the developer (Radiance Renewables), and the lender.</li>
                                                <li>The debt exposure can be on Radiance/Customer as per requirement, with the solar power plant assets being pledged to the Lender, along with step in rights to the Lender.</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr class="tablerow">
                                        <td class="Type">Overall Benefits</td>
                                        <td class="Size">
                                            <ul class="CircleBullet">
                                                <li>No upfront investment required from the customer’s end (except in the case of captive/group captive projects). </li>
                                                <li>Guaranteed savings on electricity bills from day 1 of the project's commercial operations. </li>
                                                <li>Plant performance and O&M risk borne by Radiance Renewables.</li>
                                            </ul>
                                        </td>
                                        <td class="Customers">
                                            <ul class="CircleBullet">
                                                <li>Minimal upfront investment from the customer’s end, freeing up equity for the customer’s core business.</li>
                                                <li>Tax shield from Accelerated Depreciation (AD) benefit since the asset is on the customer’s books. </li>
                                                <li>GST credit available to the customer in case of off-settable credit. </li>
                                                <li>No payment for electricity generated, except for O&M charges.</li>
                                                <li>O&M performance risk is borne by Radiance Renewables.</li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>    
                            </table>                                
                        </div>
                        <p><a href="contact-us.php" target="_blank">Get in touch</a> with our project experts to learn more!</p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">What is a Power Purchase Agreement (PPA)?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p> A Power Purchase Agreement, in short PPA, is a contract between Radiance Renewables and our customer. The agreement formalises all details with regards to terms, conditions and financials of all energy delivery, and clearly states commercial terms, for example electricity purchase tariffs. </p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">What is the duration of a Power Purchase Agreement (PPA)?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>At Radiance Renewables, we customise each project to specifically meet the needs of our clients. Therefore, we are happy to build an optimal Power Purchase Agreement for you based on your individual requirements and location. <a href="contact-us.php" target="_blank">Get in touch</a> with our project experts to start the conversation!</p>                       
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">What is my Power Supply Guarantee?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>To kick-start the customised design process, our teams at Radiance Renewables will run a simulation based on the location of your site as well as on a specific analysis of the solar radiation for your area. This process results in a <strong>Minimum Supply Estimate</strong> from our end. On basis of its outcome, our experts conceptualise a solution that will meet your energy needs, and advise you about either a partial option or a complete power requirement coverage. The electrical flow between solar and grid energy remains seamless (without any required manual intervention), so that you can focus worry-free on what you do best: running your business! <a href="contact-us.php" target="_blank">Get in touch</a> with our project experts to learn more!</p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">What is the Group Captive Model?
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>A Group Captive Models is a power procurement tool in which a solar project plant is developed for the collective usage of one or many corporate buyers. A Special Purpose Vehicle (SPV) is set up, in which you will inject 26% equity, and are collectively obligated to offtake minimum 51% of the energy generated by the solar power plant. <a href="contact-us.php" target="_blank">Get in touch</a> with our project experts to learn more!</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>