<section class="Section LiteOrangeSection TeamCardsBlock">
	<div class="container">
		<div class="TeamCards">			
			<div class="row mb5">				
				<div class="col-12">
					<div class="TopHeading">
						<h2 class="LiteOrangeBorderBottom">Board of Directors</h2>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/Manikkan-S.png" alt="">
							<div class="OnHoverBox">
								<a href="#Manikkan" class="btn open-popup-link">Read More</a>
							</div>
						</div>
						<div class="TitleHead">
							<h4>Manikkan Sangameswaran</h4>
							<p>Executive Director</p>
						</div>
					</div>
					<div id="Manikkan" class="white-popup mfp-hide">
						<h4>Manikkan Sangameswaran</h4>
						<p>Executive Director, <span>Radiance Renewables</span></p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Founder of Origin Renewables, now a part of Radiance</li>
							<li>President and Co-head Infra at ICICI Venture Fund</li>
							<li>Managing Director at Babcock & Brown India </li>
							<li>Executive Director at ABN AMRO</li>
							<li>Associate Director at UBS Securities</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/Prasanna-Desai.png" alt="">
							<div class="OnHoverBox">
								<a href="#Prasanna" class="btn open-popup-link">Read More</a>
							</div>
						</div>
						<div class="TitleHead">
							<h4>Prasanna Desai</h4>
							<p>Director</p>
						</div>
					</div>
					<div id="Prasanna" class="white-popup mfp-hide">
						<h4>Prasanna Desai</h4>
						<p>Director, Radiance Renewables</p>
						<p>MD & Head of Operations, EverSource Capital</p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/Satish-Mandhana.png" alt="">
							<div class="OnHoverBox">
								<a href="#Satish" class="btn open-popup-link">Read More</a>
							</div>
						</div>
						<div class="TitleHead">
							<h4>Satish Mandhana</h4>
							<p>Director</p>
						</div>
					</div>
					<div id="Satish" class="white-popup mfp-hide">
						<h4>Satish Mandhana</h4>
						<p>Director, Radiance Renewables</p>
						<p>Senior Managing Director & Chief Investment Officer, EverSource Capital</p>
						<ul class="CircleBullet">
							<li>Over 36 years of experience</li>
							<li>Pioneer in creating scaled platforms</li>
							<li>Formerly Managing Partner and CIO at IDFC Alternatives:</li>
						</ul>
						<ul class="TriBullet">
							<li>Made 28 investments</li>
							<li>Deployed USD 1.0 billion</li>
							<li>Full / part monetization of 26 investments</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="TopHeading">
						<h2 class="LiteOrangeBorderBottom">Management Team</h2>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/Amit_Side.png" alt="">
							<!-- <div class="OnHoverBox">
								<h5>Head of Operations</h5>
								<a href="#Amit" class="btn open-popup-link">Read More</a>
							</div> -->
						</div>
						<div class="TitleHead">
							<h4>Amit Kumar Mittal</h4>
							<p>Head of Operations</p>
						</div>
					</div>
					<div id="Amit" class="white-popup mfp-hide">
						<h4>Amit Kumar Mittal</h4>
						<p>Director, Radiance Renewables<br> <span>MD & Head of Operations, EverSource Capital<span></p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/NitinBhatia.png" alt="">
							<!-- <div class="OnHoverBox">
								<h5>Head - Corporate Finance</h5>
								<a href="#Nitin" class="btn open-popup-link">Read More</a>
							</div> -->
						</div>
						<div class="TitleHead">
							<h4>Nitin Bhatia </h4>
							<p>Head - Corporate Finance</p>
						</div>
					</div>
					<div id="Nitin" class="white-popup mfp-hide">
						<h4>Nitin Bhatia</h4>
						<p>Head - <span>Corporate Finance</span></p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/Vishal_Jain_Photo.png" alt="">
							<!-- <div class="OnHoverBox">
								<h5>Head - Project Management</h5>
								<a href="#Commander" class="btn open-popup-link">Read More</a>
							</div> -->
						</div>
						<div class="TitleHead">
							<h4>Commander Vishal Jain </h4>
							<p>Lead - Project Management</p>
						</div>
					</div>
					<div id="Commander" class="white-popup mfp-hide">
						<h4>Commander Vishal Jain </h4>
						<p>Lead - Project Management</p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/gaurav.png" alt="">
							<!-- <div class="OnHoverBox">
								<h5>Financial Controller</h5>
								<a href="#Gaurav" class="btn open-popup-link">Read More</a>
							</div> -->
						</div>
						<div class="TitleHead">
							<h4>Gaurav Srivastava </h4>
							<p>Financial Controller</p>
						</div>
					</div>
					<div id="Gaurav" class="white-popup mfp-hide">
						<h4>Gaurav Srivastava </h4>
						<p>Financial Controller</p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/Mallikarjuna.png" alt="">
							<!-- <div class="OnHoverBox">
								<h5>Head - Asset Management</h5>
								<a href="#Mallikarjuna" class="btn open-popup-link">Read More</a>
							</div> -->
						</div>
						<div class="TitleHead">
							<h4>Mallikarjuna Ukkadala </h4>
							<p>Lead - Asset Management</p>
						</div>
					</div>
					<div id="Mallikarjuna" class="white-popup mfp-hide">
						<h4>Mallikarjuna Ukkadala </h4>
						<p>Lead - Asset Management</p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/Shweta.png" alt="">
							<!-- <div class="OnHoverBox">
								<h5>Head - Business Development</h5>
								<a href="#Shweta" class="btn open-popup-link">Read More</a>
							</div> -->
						</div>
						<div class="TitleHead">
							<h4>Shweta Das </h4>
							<p>Lead - Business Development</p>
						</div>
					</div>
					<div id="Shweta" class="white-popup mfp-hide">
						<h4>Shweta Das </h4>
						<p>Lead - Business Development</p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/omkar.png" alt="">
							<!-- <div class="OnHoverBox">
								<h5>Head – Health, Safety & Environment</h5>
								<a href="#Omkar" class="btn open-popup-link">Read More</a>
							</div> -->
						</div>
						<div class="TitleHead">
							<h4>Omkar Sawant</h4>
							<p>Lead – Health, Safety & Environment</p>
						</div>
					</div>
					<div id="Omkar" class="white-popup mfp-hide">
						<h4>Omkar Sawant</h4>
						<p>Lead – Health, Safety & Environment</p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/suneera.png" alt="">
							<!-- <div class="OnHoverBox">
								<h5>Head – Legal</h5>
								<a href="#Suneera" class="btn open-popup-link">Read More</a>
							</div> -->
						</div>
						<div class="TitleHead">
							<h4>Suneera Tandon</h4>
							<p>Lead – Legal</p>
						</div>
					</div>
					<div id="Suneera" class="white-popup mfp-hide">
						<h4>Suneera Tandon</h4>
						<p>Lead – Legal</p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/Adity-hr.png" alt="">
							<!-- <div class="OnHoverBox">
								<h5>Head – Human Capital Management</h5>
								<a href="#Adity" class="btn open-popup-link">Read More</a>
							</div> -->
						</div>
						<div class="TitleHead">
							<h4>Adity Bhardwaj </h4>
							<p>Lead – Human Capital Management</p>
						</div>
					</div>
					<div id="Adity" class="white-popup mfp-hide">
						<h4>Adity Bhardwaj </h4>
						<p>Lead – Human Capital Management</p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4">						
					<div class="Cards">
						<div class="TopImg">
							<img src="assets/img/tempimg/Santosh.png" alt="">
							<!-- <div class="OnHoverBox">
								<h5>Company Secretary</h5>
								<a href="#Secretary" class="btn open-popup-link">Read More</a>
							</div> -->
						</div>
						<div class="TitleHead">
							<h4>Santosh Pawar </h4>
							<p>Company Secretary</p>
						</div>
					</div>
					<div id="Secretary" class="white-popup mfp-hide">
						<h4>Santosh Pawar</h4>
						<p>Company Secretary</p>
						<ul class="CircleBullet">
							<li>Over 25 years of experience </li>
							<li>Formerly COO at Kiran Energy Solar</li>
							<li>Previous roles include:</li>
						</ul>
						<ul class="TriBullet">
							<li>Executive Director at F&C REIT AM</li>
							<li>Director at KPMG CF</li>
							<li>Vice President at Citigroup</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>