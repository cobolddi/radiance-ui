<section class="Section GradientBlock">
	<div class="container">
		<div class="HeadingWithIconBox">
			<h2 class="WhiteText WhiteBorderBottom" data-aos="fade-in" data-aos-duration="100">The Radiance Milestones</h2>
			<div class="row">
				<div class="col-6 col-md-3" data-aos="fade-out" data-aos-duration="150">
					<div class="IconWithHeading">
						<div class="Icons">
							<img src="assets/img/gear.svg" alt="">
						</div>
						<h4>95 MW+</h4>
						<p>Operational and <br>Managed Assets</p>
					</div>
				</div>
				<div class="col-6 col-md-3" data-aos="fade-out" data-aos-duration="200">
					<div class="IconWithHeading">
						<div class="Icons">
							<img src="assets/img/mw.svg" alt="">
						</div>
						<h4>150 MW+</h4>
						<p>Pipeline</p>
					</div>
				</div>
				<div class="col-6 col-md-3" data-aos="fade-out" data-aos-duration="200">
					<div class="IconWithHeading">
						<div class="Icons">
							<img src="assets/img/map.svg" alt="">
						</div>
						<h4>Pan India <br>Presence</h4>
						<!-- <p>Pan India Presence</p> -->
					</div>
				</div>
				<div class="col-6 col-md-3" data-aos="fade-out" data-aos-duration="200">
					<div class="IconWithHeading">
						<div class="Icons">
							<img src="assets/img/customer.svg" alt="">
						</div>
						<h4>15+</h4>
						<p>Customers</p>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="LearnMore" data-aos="fade-out" data-aos-duration="500">
			<a href="#" class="WhiteButton"><span>Learn More</span></a>
		</div> -->
	</div>
</section>