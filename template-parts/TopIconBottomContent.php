<section class="Section WhoWeAre TopIconBottomContentSection">
	<div class="container">
		<div class="TopIconBottomHeading">
			<div class="row">
				<div class="col-12 col-md-3" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
					<div class="IconWithHeading">
						<div class="IconBox">
							<img src="assets/img/eco-enable.svg" alt="">
						</div>
						<h4>Vibrancy</h4>
						<p>Radiance Renewables is a dynamic firm that delivers quality, decisiveness and renewable (green energy) solutions with the help of cutting edge talent and the latest technology.</p>
					</div>
				</div>
				<div class="col-12 col-md-3" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="350">
					<div class="IconWithHeading">
						<div class="IconBox">
							<img src="assets/img/resilience.svg" alt="">
						</div>
						<h4>Sustainable</h4>
						<p>Radiance Renewables is a green business, partnering with its customers to provide economic value and offset their carbon footprint while creating sustainability across the value chain, including ecosystem partners, the environment, and investors.</p>
					</div>
				</div>
				<div class="col-12 col-md-3" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
					<div class="IconWithHeading">
						<div class="IconBox">
							<img src="assets/img/reliability.svg" alt="">
						</div>
						<h4>Reliability</h4>
						<p>Radiance Renewables is a dependable company that ensures cost and energy effectiveness as well as carbon avoidance while always being accessible to customers and partners.</p>
					</div>
				</div>
				<div class="col-12 col-md-3" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="450">
					<div class="IconWithHeading">
						<div class="IconBox">
							<img src="assets/img/economic.svg" alt="">
						</div>
						<h4>Eco-Enabling</h4>
						<p>Radiance Renewables is a resilient and flexible organisation, adapting to the needs of individual customers while creating renewable energy projects, and ensuring that they are delivered smoothly.</p>
					</div>
				</div>
			</div>
		</div>	
	</div>
</section>