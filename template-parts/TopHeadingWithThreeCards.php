<section class="Section TopHeadingThreeCards LiteOrangeSection">
	<div class="container">
		<div class="TopHeading SmallContainer" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="200">
			<h2 class="LiteOrangeBorderBottom">We reduce your project cost lifecycle.</h2>
			<p>Radiance Renewables takes up the responsibility of working together with our customers and technical partners to provide a solution that is most cost effective in three steps:</p>
		</div>
		<div class="ThreeCardsWithIcon">
			<div class="row">
				<div class="col-12 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
					<div class="Cards">
						<div class="IconBox">
							<img src="assets/img/solution-monitoring.svg" alt="">
						</div>
						<h4>Monitoring</h4>
						<p>Our experts monitor the performance of the various solar modules of the solar power generation plant through an in-house asset management team to ensure maximum power generation from the solar plants.</p>
					</div>
				</div>
				<div class="col-12 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="350">
					<div class="Cards">
						<div class="IconBox">
							<img src="assets/img/solution-quality.svg" alt="">
						</div>
						<h4>Quality</h4>
						<p>Radiance Renewables work with trusted Engineering, Procurement, Construction (EPC) partners to make sure that our plant quality is always the best in class.</p>
					</div>
				</div>
				<div class="col-12 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
					<div class="Cards">
						<div class="IconBox">
							<img src="assets/img/solution-standards.svg" alt="">
						</div>
						<h4>Standards</h4>
						<p>We deploy separate Owner’s Engineers a team of independent advocates for the owner of each project - and thereby ensuring adherence to our health, safety and environment (HSE) policies and plant quality requirements.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>