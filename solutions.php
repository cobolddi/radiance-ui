<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Radiance Renewables</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">
	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3">
				<a href="index.php" class="logo">
					<img src="assets/img/logo.svg" alt="">
				</a>
			</div>
			<div class="col-6 col-md-9">
				<div class="IpadDesktop">
					<nav>
						<ul>
							<li>
								<a href="index.php">HOME</a>
							</li>
							<li class="submenu">
								<a href="who-we-are.php">ABOUT</a>
								<ul>
									<li>
										<a href="who-we-are.php">Who are we?</a>				
									</li>
									<li>
										<a href="team.php">Our Team</a>				
									</li>
								</ul>
							</li>
							<li class="submenu">
								<a href="the-radiance-way.php">THE RADIANCE WAY</a>
								<ul>
									<li>
										<a href="the-radiance-way.php#whyradiance">Why Radiance?</a>				
									</li>
									<li>
										<a href="the-radiance-way.php#USPcards">Our USPs</a>			
									</li>
									<li>
										<a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>				
									</li>
								</ul>
							</li>
							<li>
								<a href="solutions.php">SOLUTIONS</a>
							</li>
							<li>
								<a href="portfolio.php">PORTFOLIO</a>
							</li>
							<li>
								<a href="project-faq.php">FAQs</a>
							</li>
							<li class="contactus submenu">
								<a href="contact-us.php">CONTACT</a>
								<ul>
									<li>
										<a href="contact-us.php#FormBlock">Contact Form</a>				
									</li>
									<li>
										<a href="work-with-us.php">Careers</a>			
									</li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
				<div class="IpadRemoved">
					<div class="MobileMenu">
						<button class="c-hamburger c-hamburger--htx">
					  		<span></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>						
</header>


<nav class="sub-menu open">
	<ul>
		<li>
			<a href="index.php">HOME</a>
		</li>
		<li class="haveSubmenu">
			<a href="who-we-are.php">ABOUT</a>
			<ul>
				<ul>
					<li>
						<a href="who-we-are.php">Who are we?</a>				
					</li>
					<li>
						<a href="team.php">Our Team</a>				
					</li>
				</ul>
			</ul>
		</li>
		<li class="haveSubmenu">
			<a href="the-radiance-way.php">THE RADIANCE WAY</a>
			<ul>
				<li>
					<a href="the-radiance-way.php#whyradiance">Why Radiance?</a>				
				</li>
				<li>
					<a href="the-radiance-way.php#USPcards">Our USPs</a>			
				</li>
				<li>
					<a href="the-radiance-way.php#RadianceProcess">The Radiance Process</a>				
				</li>
			</ul>
		</li>
		<li>
			<a href="solutions.php">SOLUTIONS</a>
		</li>
		<li>
			<a href="portfolio.php">PORTFOLIO</a>
		</li>
		<li>
			<a href="project-faq.php">FAQs</a>
		</li>
		<li class="haveSubmenu">
			<a href="contact-us.php">CONTACT</a>
			<ul>
				<li>
					<a href="contact-us.php#FormBlock">Contact Form</a>				
				</li>
				<li>
					<a href="work-with-us.php">Careers</a>			
				</li>
			</ul>
		</li>
	</ul>
</nav>

<main>

<section class="HomeBanner InsideBanner InsideBigBanner">
	<picture>
		<source media="(min-width:465px)" srcset="assets/img/tempimg/solution-banner.png">
		<img src="assets/img/tempimg/solution.jpg" alt="Radiance Renewable">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="BannerText" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="700">
				<h1 class="OrangeBorderBottom">Solutions</h1>
			</div>
		</div>
	</div>
</section>

<section class="Section LiteOrangeSection FourCardBlock">
	<div class="container">
		<div class="TopHeading SmallContainer" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="200">
			<h2 class="LiteOrangeBorderBottom">We offer customised solutions.</h2>
			<p>Radiance Renewables offers multiple business models based on the needs and requirements of its customers to maximize savings while complying with relevant regulatory frameworks. Throughout all our projects, we take up the responsibility of working together with our customers and technical partners to provide most cost-effective solutions while complying with best practise in performance monitoring, continuous quality management and maintaining the highest Health, Safety, Environment (HSE) policies and standards.</p>
		</div>
		<div class="FourCardWithLink">
			<div class="row">
				<div class="col-6 col-md-3" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
					<a href="project-faq.php#rightsolution">
						Open Access Captive
						<span><img src="assets/img/arw-right.svg" alt=""></span>
					</a>
				</div>
				<div class="col-6 col-md-3" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
					<a href="project-faq.php#rightsolution">
						Open Access Third Party
						<span><img src="assets/img/arw-right.svg" alt=""></span>
					</a>
				</div>
				<div class="col-6 col-md-3" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="500">
					<a href="project-faq.php#rightsolution">
						Behind-the-Meter (Rooftop or on-premises installations)
						<span><img src="assets/img/arw-right.svg" alt=""></span>
					</a>
				</div>
				<div class="col-6 col-md-3" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="600">
					<a href="project-faq.php#rightsolution">
						Deferred Capex Model
						<span><img src="assets/img/arw-right.svg" alt=""></span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/DarkBgSectionWithHeading.php') ?>

<section class="Section LifeCycleThreeCardsBlock">
	<div class="container">
		<div class="TopImage DesktopOnly">
			<img src="assets/img/lifecycle.png" alt="" class="lifecycle DesktopOnly" data-aos="fade-out" data-aos-easing="linear" data-aos-duration="200">
		</div>
		<div class="Lifecycle MobileOnly">
			<!-- <div class="Toparw">
				<h4>Land. Evacuation & Approvals</h4>
				<img src="assets/img/top-arrow.png" alt="">
			</div> -->
			<div class="LifecycleSlider">
			  	<div class="Cards">
					<div class="topBlock">
						<p>Project<br> Evaluation</p>
						<p>Customised<br> Design</p>
					</div>
					<div class="bottomBlock">
						<p class="leftbox">Understand <br>sustainability<br> and customer <br>needs</p>
						<p class="rightbox">Conceptualise <br>long-term<br> renewable energy<br> solution</p>
					</div>
				</div>
				<div class="Cards lifecycle">
					<div class="topBlock">
						<p>Engineering &<br> procurement</p>
						<p>Construction &<br> Commissioning</p>
					</div>
					<div class="bottomBlock">
						<p class="leftbox">Best in class<br> components and<br> expert led site<br> planning</p>
						<p class="rightbox">High- quality<br> installation and<br> seamless<br> connection</p>
					</div>
				</div>
				<div class="Cards">
					<div class="topBlock">
						<p>Operations &<br> Maintenance</p>
						<p>Asset <br> Management</p>
					</div>
					<div class="bottomBlock">
						<p class="leftbox">Optimizing <br>Performance and <br>insuring long term/ <br>lifecycle efficiency <br>of installation</p>
						<p class="rightbox">Focus on output, <br>learning, effective <br>management and <br>corporate goverance</p>
					</div>
				</div>
			</div>
			<!-- <div class="BottomArw">
				<img src="assets/img/bottom-arrow.png" alt="">
				<h4>Financing</h4>
			</div> -->
		</div>
		<div class="ThreeCardsBlock">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="CardsBlock">
						<div class="LeftIconRightContent" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
							<div class="icon">
								<img src="assets/img/phase1.svg" alt="">
							</div>
							<h4>Phase 1:<br><span>Evaluating, Design Thinking & Planning</span></h4>
						</div>
						<div class="BtmBoxBorder" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
							<p><strong>Outcome</strong>: Recommending the correct and customised renewables solution for our customers</p>
							<ul>
								<li>Studying the requirements of our customer</li>
								<li>Site visits and inspection by our technical team</li>
								<li>Designing of the most appropriate system taking into consideration the technical, commercial & regulatory requirements and constraints</li>
								<li>Site orientation and shadow effects</li>
								<li>Power evacuation modes</li>
								<li>Fire safety and security requirements in line with standards</li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="CardsBlock">
						<div class="LeftIconRightContent" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="350">
							<div class="icon">
								<img src="assets/img/phase2.svg" alt="">
							</div>
							<h4>Phase 2:<br><span>Engineering & Installing</span></h4>
						</div>
						<div class="BtmBoxBorder" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="350">
							<p><strong>Outcome</strong>: Setting up the renewable solutions on or off site</p>
							<ul>
								<li>Engineering, procuring, installing, integrating and commissioning of the solar power system at the customer site or at an offsite location for open access projects</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="CardsBlock">
						<div class="LeftIconRightContent" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
							<div class="icon">
								<img src="assets/img/phase3.svg" alt="">
							</div>
							<h4>Phase 3:<br><span>Proactive Asset Management</span></h4>
						</div>
						<div class="BtmBoxBorder" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
							<p><strong>Outcome</strong>: Operating and maintaining the renewable energy plant in a smooth, efficient and best in class manner with zero headache for the customer</p>
							<ul>
								<li>Proactive Asset Management of the renewable energy assets over the contracted period</li>
								<li>OPEX optimization with increased efficiency & energy delivery</li>
								<li>Optimal Operations with an aim to maximize production with minimized cost</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="Section LiteOrangeSection ThreeServiceCardsBlock">
	<div class="container">
		<div class="TopHeading SmallContainer" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="200">
			<h2 class="LiteOrangeBorderBottom">Futuristic Solutions</h2>
			<p>Radiance Renewables also offers services in the following areas:</p>
		</div>
		<div class="ThreeServiceCards">
			<div class="row">
				<div class="col-12 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="300">
					<div class="IconCards">
						<img src="assets/img/solarbattery.svg" alt="">
						<h4>Solar and Battery Storage/Hydrogen<br> Hybrid Solutions</h4>
						<ul>
							<li>Commercial and Industrial Greenfield Development and Operational Infrastructure Assets/Platforms</li>
							<li>Customized Solutions as per client’s requirements</li>
							<li>Proven and recognized technology solutions and services</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="400">
					<div class="IconCards">
						<img src="assets/img/windsolar.svg" alt="">
						<h4>Wind and Solar Hybrid Solar Solutions</h4>
						<ul>
							<li>Commercial and Industrial Greenfield Development and Operational Infrastructure Assets/Platforms</li>
							<li>Supply & Integrating Hybrid power for lower Levelized Cost of Electricity</li>
							<li>Personalised turnkey projects, Engineering, Procurement and Construction scope and Operations and Maintenance</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-4" data-aos="fade-in" data-aos-easing="linear" data-aos-duration="500">
					<div class="IconCards">
						<img src="assets/img/geberation.svg" alt="">
						<h4>Other forms of Generation</h4>
						<ul>
							<li>Hydro/Geothermal</li>
							<li>Choosing best in class energy source</li>
							<li>Constructive use of waste for reducing CO2 emissions</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>









<?php @include('template-parts/footer.php') ?>